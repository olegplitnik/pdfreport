
const express = require('express');
const app = express();
const pug = require('pug');
const pdf = require('html-pdf');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
const HttpStatus = require('http-status-codes');
const swaggerize = require('swaggerize-express');

//routes
var routePdf = require('./routes/pdf');

// parse application/json

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

//swaggerize
// app.use(swaggerize({
//     api: path.resolve('./config/api.yaml'),
//     docspath: '/swagger/docs/v1', //http://localhost:3000/swaggerui/?url=/swagger/docs/v1
//     handlers: path.resolve('./routes')
// }));

app.use('/swaggerui', express.static('./node_modules/swagger-ui/dist'));

app.set('view engine', 'pug')

app.get('/', function (req, res) {
    res.render('index', {})
});

app.use('/', routePdf);

app.use('/swaggerui', express.static('./node_modules/swagger-ui/dist'));

app.set('view engine', 'pug')


app.listen(3000, function () {

});