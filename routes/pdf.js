const express = require('express');
const router = express.Router();
const pdf = require('html-pdf');
const HttpStatus = require('http-status-codes');
const async = require('async');
const fs = require('fs');
const pug = require('pug');

router.post('/pdf', function (req, res) {

    var template = req.body.template;
    var templatePayload = req.body.payload;

    if (template == null) {
        res.status(HttpStatus.BAD_REQUEST).send({ message: "template is required" })
        return;
    }

    if (templatePayload == null) {
        res.status(HttpStatus.BAD_REQUEST).send({ message: "payload is required" })
        return;
    }

    var templatesFolder = "./views/reports/";

    async.waterfall([

        (next) => { fs.readdir(templatesFolder, next); },

        (files, next) => {

            var templateIndex = files.indexOf(template + ".pug");

            if (templateIndex < 0) {
                next({ message: "template not found" });
                return;
            }
            next(null, templatesFolder + template + ".pug");
        },

        (templateFile, next) => {
            const compiledFunction = pug.compileFile(templateFile);
            var html = compiledFunction(templatePayload);
            pdf.create(html).toBuffer(next);

        }


    ], (err, pdfBuffer) => {

        if (err) {
            res.status(HttpStatus.BAD_REQUEST).send(err);
            return;
        }

        res.writeHead(HttpStatus.OK, { 
            'Content-Type': 'application/pdf',
            'Content-Disposition': 'attachment; filename="' + template + '.pdf"'
         });
        res.end(pdfBuffer, 'binary');

    });



});


module.exports = router;