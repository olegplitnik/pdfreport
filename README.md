## Usage

### Prerequisites

* Node.js >= 4.1.2 and npm installed

### Running

1. Download repository either by using `git clone`, or download and unzip repository.
2. cd to repo root directory (directory with `package.json`) and run `npm install`