const assert = require('assert');
const request = require('request');
const server = require('../app')
const HttpStatus = require('http-status-codes');


describe('Pdf', () => {

  describe('POST /pdf', () => {

    var postOptions = {
      uri: "http://localhost:3000/pdf",
      body: {},
      json: true
    }

    beforeEach(() => {
      postOptions.body = {}
    });



    it('should return 400 code if template not provided', (done) => {
      request.post(postOptions, (err, responce, body) => {
        assert(responce.statusCode == HttpStatus.BAD_REQUEST);
        done();
      })
    });

    it('should return 400 code if payload not provided', (done) => {
      postOptions.body = { template: 'test' };
      request.post(postOptions, (err, responce, body) => {
        assert(responce.statusCode == HttpStatus.BAD_REQUEST);
        done();
      })
    });

    it('should return 400 code if template not found', (done) => {
      postOptions.body = { template: 'some random template name', payload: {} };
      request.post(postOptions, (err, responce, body) => {
        assert(responce.statusCode == HttpStatus.BAD_REQUEST);
        done();
      })
    });

    it('should return 200 code and pdf if template and payload provided', (done) => {

      postOptions.body = {
        template: 'test',
        payload: {}
      }

      request.post(postOptions, (err, responce, body) => {
        assert(responce.statusCode == HttpStatus.OK);
        done();
      })

    }).timeout(15000);


  });
});